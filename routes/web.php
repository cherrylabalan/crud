<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'UserController@index');**/

Route::get('/user', 'UserController@index');

Auth::routes();

Route::get('/user/add','UserController@add');
Route::post('/user/add','UserController@create');

Route::get('/user/{id}','UserController@edit');
Route::post('/user/{id}','UserController@update');

Route::delete('/user/delete/{id}', 'UserController@delete');