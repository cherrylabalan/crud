@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Add New User</h2>

        <form method="POST" action="{{action('UserController@create')}}">

            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Firstname</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="firstname" name="first_name">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Lastname</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="lastname" name="last_name">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Address</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="address" name="address">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Postcode</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="postcode" name="postcode">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Contact Number</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="contact_num" name="phone_num">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="email" name="email">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Username</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="username" name="username">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Password</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="" name="password">
                </div>
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add User</button>
            </div>
            {{ csrf_field() }}
        </form>

    </div>
@endsection