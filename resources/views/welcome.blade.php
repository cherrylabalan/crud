@extends('layouts.app')

@section('content')
<div class="container">
    <h2>User List</h2>
    <a href="{{action('UserController@add')}}" class="btn btn-primary">Add new User</a>
    <table class="table">
        <thead>
        <tr>
            <th>First name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Postcode</th>
            <th>Contact Number</th>
            <th>Email</th>
            <th>Username</th>
            <th></th>
        </tr>
        </thead>
        <tbody>@foreach($users as $user)
            <tr>
                <td>
                    {{$user['first_name']}}
                </td>
                <td>
                    {{$user['last_name']}}
                </td>
                <td>
                    {{$user['address']}}
                </td>
                <td>
                    {{$user['postcode']}}
                </td>
                <td>
                    {{$user['phone_num']}}
                </td>
                <td>
                    {{$user['email']}}
                </td>
                <td>
                    {{$user['username']}}
                </td>
                <td>
                    <a href="{{action('UserController@edit', $user['id'])}}" class="btn btn-warning">Edit</a>
                    <form action="{{action('UserController@delete', $user['id'])}}" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach</tbody>
    </table>
</div>
@endsection