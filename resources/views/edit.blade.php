@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit the User</h1>

        <form method="POST" action="{{action('UserController@update', $id)}}">

            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Firstname</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="firstname" name="first_name" value="{{$user['first_name']}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Lastname</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="lastname" name="last_name" value="{{$user['last_name']}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Address</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="address" name="address" value="{{$user['address']}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Postcode</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="postcode" name="postcode" value="{{$user['postcode']}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Contact Number</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="contact_num" name="phone_num" value="{{$user['phone_num']}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="email" name="email" value="{{$user['email']}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Username</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="username" name="username" value="{{$user['username']}}">
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update User</button>
            </div>
            {{ csrf_field() }}
        </form>



    </div>
@stop