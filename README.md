### Installation:

* Clone this repository
* Go to root directory of the project and run command `composer install`
* Create `.env` file from `.env.example`
* Edit `.env` file using your MySQL database credentials
* Run the command `php artisan migrate`
* After DB migration is done, run `php artisan serve`
* Go to this URL: `http://localhost:8000/user`

*If you encounter an error regarding encryption key, run command `php artisan key:generate`*
