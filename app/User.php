<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\DB;

class User extends Model
{
    protected $table = 'user';
    protected $fillable = ['first_name','last_name','address','postcode','phone_num','email','username','password'];

}
