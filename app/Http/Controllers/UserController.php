<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UserController extends Controller
{
    public function index()
    {
        $users = User::all()->toArray();
        return view('welcome', compact('users'));
    }

    public function add()
    {
        return view('add');
    }

    public function create(Request $request)
    {
        $user = new User($request->all());
        $user->save();
        return redirect('/user');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('edit', compact('user','id'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $param = $request->all();
        $user->update($param);
        return redirect('/user');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/user');
    }
}
